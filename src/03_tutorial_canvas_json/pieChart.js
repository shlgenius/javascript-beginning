// This is how you structure JSON data
// var data = '{"expenditures":[{"type":"Healthcare", "percent":"8"},{"type":"Insurance", "percent":"12"},{"type":"Food", "percent":"13"},{"type":"Housing", "percent":"33"},{"type":"Transportation", "percent":"16"},{"type":"Apparel", "percent":"3"},{"type":"Entertainment", "percent":"5"},{"type":"Healthcare", "percent":"8"},{"type":"Other", "percent":"2"}]}';

// // if you want it to actually create the class itself
// // class is a blueprint of creating objects
// class Expenditures {

//     // create a custom class to create those expenditures
//     constructor(type, percent){

//         // everytime you want to create a new expenditure and store that data
//         // this.type equal to the type that it was passed inside of it
//         this.type = type,
//         this.percent = percent
//     }
// }



// ================= DRAW PIE CHART =================

// focus on how are we going to get that JSON data pull it in and turn it into a pie chart

// first we need an array that it is going hold all of that JSON data
// Will hold JSON data pulled from the textarea
let data;

// Will hold array of expenditures objects from the json data
let expenditureArray = [];

// Will hold array of percentages converted so they can be used to draw the pie chart
let percentArray = [];

// Will hold randomly generated color hex codes
let colorArray = [];


// main function
function drawChart(){

    // Get data pasted into textarea
    data = document.getElementById('json-data').value;

    // Will hold the calculated percentages of the pie each category will take up
    // call a function that is going to change the percentages inside the expenditures into a format 
    // that is going to able to be used to create pie charts
    percentArray = createPercentArray();

    // Will store an array of random color hex codes
    // populate the color array
    colorArray = createRandomColorArray();

    // Will convert JSON data into an array of objects
    populateArray(data);

    // Draws pie chart and lables
    drawPie();
}

// Pulls data from JSON and saves it into array expenditureArray
// it gets passed the json data
function populateArray(jsonData){

    // Create an array populated with expense data
    // convert the json data into a regular javascript array 
    let expenseArray = JSON.parse(jsonData);

    // Cycle through data and put it in our expenditures array
    // expenseArray and then you have to reference the actual array name inside of the json data
    // and that is going to be expenditures 
    // and add length to find out how many items are in the array
    for (i = 0; i < expenseArray.expenditures.length; i++){

        // Get each object in the array
        let expense = expenseArray.expenditures[i];
        expenditureArray[i] = expense;
    }
    // that pulls the json data out and sticks it in array we want    
}

// Create an array where we convert our percentages so that they total up to 2 [100 * .02 = 2]
// but we need them to add up to 2
function createPercentArray(){
    let perArr = [];

    for(i = 0; i < expenditureArray.length; i++){
        perArr[i] = expenditureArray[i].percent * .02;
    }
    return perArr;
}

function createRandomColorArray(){
    let randColorArr = [];

    for(i = 0; i < expenditureArray.length; i++){

        // Generate random base 16 hex code 16777215 = FFFFFF
        // toString(16) returns base 16 of provided number
        randColorArr[i] = '#' + Math.floor(Math.random() * 16777215).toString(16);
    }
    return randColorArr;
}

function drawPie(){

    // first we should
    // Get reference to canvas 
    var canvas = document.getElementById('canvas');

    // Gives you the ability to draw 2D shapes, text, images, etc.
    // need to reference the canvas and getContext
    // and we want to draw 2 dimensional objects on it ('2d')
    var context = canvas.getContext('2d');

    // Start drawing arc at 0
    // start drawing the arc of the curve that is going to be applied to each of the pie slices
    let startAngle = 0;
    let endAngle = 0;

    // now draw all of it
    for (i = 0; i < percentArray.length; i++){

        // Start the next arc where the last arc left off
        startAngle = endAngle;
        endAngle = endAngle + (percentArray[i] * Math.PI);

        drawSlice(context, 300, 200, 150, startAngle, endAngle, colorArray[i]);
        // context : gives the ability to draw on the canvas
        // 300 : x point for the center of the circle
        // 200 : y point for the center of the circle
        // 150 : radius of the circle
        // startAngle : starting angle for the curve for each slice
        // endAngle : ending angle
        // colorArray[i] : individual color array the color you want to draw the slice with

        // Have to multiply by 50 because original data was multiplied times .02
        drawSliceText(context, 300, 200, 150, startAngle, endAngle, percentArray[i] * 50);        
    }
}

function drawSlice(context, sliceCenterX, sliceCenterY, radius, startAngle, endAngle, color){

    // fillStyle : Color, Gradient or Pattern used to fill shapes
    // define the fill color for the slice
    context.fillStyle = color;

    // Signal that you want to start drawing a path
    context.beginPath();


    // ======== Code used to separate slices ========
    // Get median angle
    let medianAngle = (startAngle + endAngle) / 2;

    // Get offset amount from center
    // Increace multiple to increase offset 
    xOffset = Math.cos(medianAngle) * 30;
    yOffset = Math.sin(medianAngle) * 30;

    // ==============================================


    // Define start of drawing area (move to the starting drawing area)
    // Delete offsets to have a normal pie chart
    // context.moveTo(sliceCenterX, sliceCenterY);
    context.moveTo(sliceCenterX + xOffset, sliceCenterY + yOffset);

    // Draw arc for pie slice
    // context.arc(sliceCenterX, sliceCenterY, radius, startAngle, endAngle);
    context.arc(sliceCenterX + xOffset, sliceCenterY + yOffset, radius, startAngle, endAngle);

    // Close the pie slice
    context.closePath();

    // Fill the pie slice
    context.fill();
}

function drawSliceText(context, sliceCenterX, sliceCenterY, radius, startAngle, endAngle, percentText){

    // Draw Text
    // Get Median Angle : (startAngle + endAngle)/2
    // I define where to place text using polar coordinates
    // They are defined in Cartesian coordinates using
    // x = radius * cos(angular coordinate)
    // y = radius * sin(angular coordinate)
   
    let textX = sliceCenterX + Math.cos((startAngle + endAngle) / 2) * radius;
    let textY = sliceCenterY + Math.sin((startAngle + endAngle) / 2) * radius;

    context.fillStyle = 'white';
    context.font = '20px Calibri';
    context.fillText(percentText, textX, textY);

}