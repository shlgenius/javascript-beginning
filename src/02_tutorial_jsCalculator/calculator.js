// preVAl; newVal; operator; resultVal;

let preVal = "";                // Stores previous value if any
let newVal = "";                // Stores value we are building
let resultVal = "";             // Stores value to display in the entry window
let mathOperator = "";          // Store previous math operator clicked

// stop the user from clicking on the decimal button more than once
// [Only allow 1 per new value entered]
// set false to start because obviously whenever it opens the user haven't clicked on it
let decimalClicked = false;     // Store whether decimal has been clicked

let valMemStored = "";          // Will hold values we want to store in memory


// now we have to create all of the functions

// Called when a number button is clicked
function numBtnPress(num){

    // Check if a number has been already clicked
    // check whether the resultVal has a value or not and if it does create a new value
    if(resultVal){
        newVal = num;           // Start a new new number
        resultVal = "";         // Reset to create a new result
    } else {

        // Used to block using multiple decimals
        if(num == '.'){
            if(decimalClicked != true){
                newVal += num;
                decimalClicked = true;
            } 
        } else {
            newVal += num;
        }
    }
    // after the number has been pressed we need to update the entry
    // Update value in entry input tag
    document.getElementById("entry").value = newVal;
}

function mathBtnPress(operator){

    // Check if there was a previous calculation by seeing if resultVal has a value
    // If result doesn't have a value then store the current val as a previous for the next calculation

    // check if result doesn't have a value then we are going to store the current value
    // as a previous for the next calculation    
    if(!resultVal){                // if it doesn't have a value
        preVal = newVal;
    } else {                       // if there is a previous result

        // If there is a current result store that as the previous value entered
        preVal = resultVal;
    }

    // Restart creation of new number
    newVal = "";

    // Reset decimalClicked
    // because we are working with the creation of a new value once again so reset that to false
    decimalClicked = false;

    // Store operator clicked
    mathOperator = operator;
    
    // Prepare entry for receiving new numbers
    resultVal = "";
    document.getElementById("entry").value = "";

}

function equalBtnPress(num){
    // Reset decimalClicked
    decimalClicked = false;

    // Convert string numbers to floats
    preVal = parseFloat(preVal);
    newVal = parseFloat(newVal);

    // Perform calculations based on stored operator
    switch(mathOperator){
        case "+":
            resultVal = preVal + newVal;
            break;
        case "-":
            resultVal = preVal - newVal;
            break;
        case "*":
            resultVal = preVal * newVal;
            break;
        case "/":
            resultVal = preVal / newVal;
            break;

        // If equals is hit without an operator leave everything as is
        default:
            resultVal = newVal;
    }

    // Store the current value as the previous so that you can except to next value in the calculation
    preVal = resultVal;

    // Put the calculation result in the entry window
    document.getElementById("entry").value = resultVal;
}

// Clear everything except memory
function clearBtnPress() {
    preVal = "";
    newVal = "";
    resultVal = "";
    mathOperator = "";
    decimalClicked = false;
    document.getElementById("entry").value = "0";
}

// Store the current value in the entry window
function copyBtnPress(){
    valMemStored = document.getElementById("entry").value;
}

// If a value has been stored paste it in the entry window and assign it as the newVal
function pasteBtnPress(){
    if(valMemStored){
        document.getElementById("entry").value = valMemStored;
        newVal = valMemStored;
    }
}