// You can create custom ranges with Array.from
// Define how many items to generate and the arrow 
// function to pop out values from 0 to 4
let a1 = Array.from({length: 5}, (x, i) => i);
// we tell it that we want it to generate an array that has a length of 5 elements
// use the arrow operator to automatically generate values from 0 to 4 to place it inside the array
// the (x, i)
// we are generating an array which is going to contain 5 values as we defined
// and whenever the array is created with these 5 spaces
// each of those spaces inside of the array are going to have a value of undefined
// which is going to be represented by x
// but that is the information that we do not need
// i then in this situation is going to have an increasing value of 0 through 4
// and that is how we are going to assign those values to our brand new array

console.log(a1);


// This can be abbreviated
// The rest parameter ... binds to the array that will contain the numbers 0 - 4
// keys() returns the generated list
let a2 = [...Array(5).keys()];
// Array(5) we want to generate 5 items inside the array
// after that we will call keys() to return the generated list of 5 items

console.log(a2);


// Create a custom array with start, stop and step
const range = (start, stop, step) => Array.from({length: (stop - start) / step + 1}, (x,i) => start + (i * step));
// define a function and assign it to the variable over range and define our attributes (start, stop, step)
// generate an array of a certain length and calculate that length
// throw x inside which is going to be used to create the undefined elements inside of the array
// use arrow operator to populate those undefined elements
// ( 50 - 10 ) / 10 + 1 = 5
// 10 + 0, 10 + 10, ....

// now we should actually create an array calling range
let a3 = range(10,50,10);
console.log(a3);


// Use arrow operator to make 5 squares
// The array is created with 5 spaces with the value undefined represented by x
// i has increasing values from 0 to 4 and we get the square of each and then reassign to the array
// lets say you want to generate an array of square values
let a4 = Array.from({length: 5}, (x, i) => i * i);
console.log(a4);


// You can use for in to cycle through the array
for (let i in a4) {
    console.log(i);
}


// ========= Compound Interest =========
// Each year their investment will increase by their 
// AccountValue = investment + their investment * the interest rate

// This function returns the year end new value depending on a changing interest rate
// Account Value = Principle(1 + rate)^years
function getAccountValue(principle, interestRate, afterYear){
    return (principle * (Math.pow(1 + interestRate, afterYear))).toFixed(2);
}

function getTable(){

    // Get the requested interest rates and convert to percents for calculations
    let rate1 = document.getElementById("rate1").value * .01;
    let rate2 = document.getElementById("rate2").value * .01;
    let rate3 = document.getElementById("rate3").value * .01;
    let rate4 = document.getElementById("rate4").value * .01;

    // Calculate 5 years of returns for each rate
    let return1 = [...Array(5).keys()].map(x => getAccountValue(1, rate1, x));
    // an array and generate 5 values call keys to get those values 
    // then use map which is going to take each of those values inside of the array I just created
    // x is going to represent each individual element that takes out of the array
    // and then pass it in to the getAccountValue()
    // and then pass in 1, 
    // whatever the intrest rate is for rate1, 
    // x which is going to be these specific year that I am going to be looking for
    // which is a reference to the item in the array

    let return2 = [...Array(5).keys()].map(x => getAccountValue(1, rate2, x));
    let return3 = [...Array(5).keys()].map(x => getAccountValue(1, rate3, x));
    let return4 = [...Array(5).keys()].map(x => getAccountValue(1, rate4, x));
    
    // now generate the tables
    // get a reference to the div that I want to put my table in
    // Get the div to put the table in
    var tableArea = document.getElementById('interest-tbl');

    // Create a table element
    var investTable = document.createElement('table');

    // Add class to table element
    // the class is going to add striping to my table
    investTable.setAttribute('class', 'table table-striped');

    // Create header
    // <thead>
    //      <tr>
    //          <th>, <th>
    var thead = document.createElement('thead');
    var theadTR = document.createElement('tr');

    // Create th elements
    // appendChild because I want to add them to the <tr>    
    var th1 = theadTR.appendChild(document.createElement('th'));
    var th2 = theadTR.appendChild(document.createElement('th'));
    var th3 = theadTR.appendChild(document.createElement('th'));
    var th4 = theadTR.appendChild(document.createElement('th'));
    var th5 = theadTR.appendChild(document.createElement('th'));

    // Create th elements
    th1.innerHTML = 'Year';
    th2.innerHTML = Math.floor((rate1 * 100)) + '%';
    th3.innerHTML = Math.floor((rate2 * 100)) + '%';
    th4.innerHTML = Math.floor((rate3 * 100)) + '%';
    th5.innerHTML = Math.floor((rate4 * 100)) + '%';

    // Add the <th> elements to the <tr> elements
    theadTR.prepend(th1, th2, th3, th4, th5)

    // Add <tr> to <thead>
    thead.appendChild(theadTR)

    // <tbody>
    //      <tr>
    //          <th>
    var tbody = document.createElement('tbody');

    // cycle through all of the changing interest rates
    for(var i = 1; i < 5; i++){
        var tbodyTR = document.createElement('tr');
        var tbodyth1 = tbodyTR.appendChild(document.createElement('th'));
        var tbodyth2 = tbodyTR.appendChild(document.createElement('th'));
        var tbodyth3 = tbodyTR.appendChild(document.createElement('th'));
        var tbodyth4 = tbodyTR.appendChild(document.createElement('th'));
        var tbodyth5 = tbodyTR.appendChild(document.createElement('th'));

        // assign the actual value
        tbodyth1.innerHTML = i;        
        tbodyth2.innerHTML = return1[i];
        tbodyth3.innerHTML = return2[i];
        tbodyth4.innerHTML = return3[i];
        tbodyth5.innerHTML = return4[i];

        // prepend all of the individual value inside the tr tag
        tbodyTR.prepend(tbodyth1, tbodyth2, tbodyth3, tbodyth4, tbodyth5);

        // which is where we are going to add all of our trs that contains the ths back to the tbody tag
        tbody.appendChild(tbodyTR);
    }

    // create final table using all those individual parts
    investTable.appendChild(thead);
    investTable.appendChild(tbody);

    // assign newly created table to the tableDiv
    var tableDiv = document.getElementById("interest-tbl");
    tableDiv.appendChild(investTable);
}