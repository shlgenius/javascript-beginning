// Radius of circle
const RADIUS = 200;

// Circle center points
const X_CIRCLE_CENTER = 300;    
const Y_CIRCLE_CENTER = 300;

let canvas;

// all of the methods and properties used to manipulate the canvas
let context;

// to track mouse position create a class to store both X and Y positions for the mouse inside of class
class MousePosition {
    constructor(x, y){

        // this : referring to the mousePosition object that I create
        // and set the x to whatever the x is
        this.x = x;
        this.y = y;
    }
}

// and then create a mouse position object and store x and y position for the mouse 
// Holds changing mouse position
let mousePos = new MousePosition(0, 0);

// Call for our function to execute when page is loaded
// whenever the web page completely loads I am going to need to draw my canvas on the screen
document.addEventListener('DOMContentLoaded', setupCanvas);
// whenever the web page is fully loaded which is going to be defined as 'DOMContentLoaded'
// we are going to set up or draw a basic canvas

// now think all of the functions I need
// 1. square outside of the canvas, 2 lines, 1 circle
// 2. constantly track the mouse position
// 3. redraw the canvas every time the mouse moves
// 4. need to draw a rectangle on the screen
// 5. show text on the screen
// 6. figure out the angle of the triangle
// 7. convert degrees to radians and radians to degrees


// ==================== Creat required functions ====================

// define canvas, create an EventListener which is going to listen for mouse movements
// function setupCanvas(){}

// draw the canvas, square outside the circle, the lines 
// function drawCanvas(){}

// redraw the canvas which is going to happen every time the mouse moves
// function reDrawCanvas(){}

// draw rectangles
// function drawRectangle(){}

// draw circles
// function drawCircle(){}

// draw lines
// function drawLine(){}

// draw text
// function drawTextAtPoint(){}

// get mouse position
// function getMousePosition(){}

// get the angle using both x and y positions and we need x and y for that
// function getAngleUsingXAndY(x,y){}

// convert radians to degrees
// function radiansToDegrees(rad){}

// convert degrees to radians
// function degreesToRadians(degree){}

// draw triangles
// function drawTriangle(){}

// get line length to calculate the length of the hypotenuse and the opposite line
// function getLineLength(){}

// ==================================================================


// define canvas, create an EventListener which is going to listen for mouse movements
function setupCanvas(){

    // Get reference to canvas element
    // get a reference to the canvas
    canvas = document.getElementById("my-canvas");

    // Get methods for manipulating the canvas
    // get the context and it will get all the methods and properties that I need to able to work with my canvasand draw on it
    // drawing in 2d
    context = canvas.getContext("2d");

    // draw the canvas the way that it looks whenever the web page first loads
    drawCanvas();

    // Execute redrawCanvas when the mouse moves
    // listen to mouse movements
    // every single time the mouse moves it is going to call the function reDrawCanvas
    // because the canvas needs to be drawn, deleted, redrawn 
    // otherwise it will draw over itself
    canvas.addEventListener("mousemove", reDrawCanvas);
}

// draw the canvas, square outside the circle, the lines 
function drawCanvas(){

    // Create border
    drawRectangle("#BDBDBD", 5, 0, 0, 600, 600);

    // Create circle
    drawCircle("#BDBDBD", 1, X_CIRCLE_CENTER, Y_CIRCLE_CENTER, RADIUS, 0, 2 * Math.PI);

    // Create guide lines
    drawLine("#BDBDBD", 1, X_CIRCLE_CENTER, 0, X_CIRCLE_CENTER, 600);
    drawLine("#BDBDBD", 1, 0, Y_CIRCLE_CENTER, 600, Y_CIRCLE_CENTER);
}

// redraw the canvas which is going to happen every time the mouse moves
// created after drawText
function reDrawCanvas(){

    // Clear canvas
    // 0, 0 : start from the top of the screen
    // top of the screen to canvas.width and canvas.height
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Redraw canvas
    // basically draw in all the rectangles and the basics of the canvas
    drawCanvas();

    // Call for MousePosition object to be updated 
    // get the mouse position object to be updated
    // and that is going to have an event
    // and each time reDrawCanvas is called because it is tied to an eventListener above
    // it is going to receive an event
    getMousePosition(event);
    // now create function getMousePosition(evt)

    // Draw x & y coordinates using updated MousePosition settings
    // now we got the mousePosition by creating function radains and degrees
    // now draw my x and y coordinates using my updated mouse position settings
    drawTextAtPoint("X : " + mousePos.x, 15, 25);
    drawTextAtPoint("Y : " + mousePos.y, 15, 45);
    // checked on web page

    // Get angle in degrees and draw text
    // need to get the angle in degrees and also draw that text on the screen
    let angleOfMouseDegrees = getAngleUsingXAndY(mousePos.x, mousePos.y);
    drawTextAtPoint("Degrees : " + angleOfMouseDegrees, 15, 65);
    // checked on we page

    // Calculates hypotenuse and opposite sides and draws them
    // now need to figure out how to draw a triangles
    drawTriangle(angleOfMouseDegrees);
}

// draw rectangles
// 1. what are we going to pass in
// 2. context
// 3. put the pass information in function drawCanvas() 
function drawRectangle(strokeColor, lineWidth, startX, startY, endX, endY){
    context.strokeStyle  = strokeColor;
    context.lineWidth = lineWidth;
    context.strokeRect(startX, startY, endX, endY);
}

// draw circles
function drawCircle(strokeColor, lineWidth, xCircleCenter, yCircleCenter, radius, arcStart, arcEnd){
    context.strokeColor = strokeColor;
    context.lineWidth = lineWidth;

    // start defining that I am ready to start drawing my circle
    context.beginPath();

    // draw it as an arc
    context.arc(xCircleCenter, yCircleCenter, radius, arcStart, arcEnd);
    
    // actually draw the circle after defining everything
    context.stroke();
}

// draw lines
function drawLine(strokeColor, lineWidth, xStart, yStart, xEnd, yEnd){

    // Move to start of line
    context.moveTo(xStart, yStart);

    // Move to end of line
    context.lineTo(xEnd, yEnd);

    // then stroke the line
    // Draw line
    context.stroke();
}

// draw text
// this : is going to receive the text I am going to draw
// x, y : x and y position for draw text
function drawTextAtPoint(text, x, y){

    // define font
    context.font = "15px Arial";

    // actually draw the text on the screen
    context.fillText(text, x, y);
}

// get mouse position
// first it is going to receive an event
function getMousePosition(event){

    // get the canvas size and position relative to the web page
    // Get the canvas size and position relative to the web page
    let canvasDimensions = canvas.getBoundingClientRect();
    // now can get the canvas x and y positions

    // store x and y position inside the mousePosition object
    // x position = event information - canvasDimensions.left
    // Get canvas x & y position
    mousePos.x = Math.floor(event.clientX - canvasDimensions.left);
    mousePos.y = Math.floor(event.clientY - canvasDimensions.top);

    // need to convert to our coordinate graph I set up
    // Convert to coordinate graph
    mousePos.x -= 300;
    mousePos.y = -1 * (mousePos.y - 300);

    // return mousePosition updated object
    return mousePos;
}

// get the angle using both x and y positions and we need x and y for that
// figure out what the angle is using x and y
// Returns the angle using x and y
// x = Adjacent Side
// y = Opposite Side
// Tan(Angle) = Opposite / Adjacent
// Angle = ArcTan(Opposite / Adjacent)
function getAngleUsingXAndY(x,y){
    let adjacent = x;
    let opposite = y;

    // atan2 : tangent
    return radiansToDegrees(Math.atan2(opposite, adjacent));
}


// radians & degrees after getMousePosition

// convert radians to degrees
function radiansToDegrees(radians){

    // if radians comes in negative need to change the results
    if(radians < 0){

        // Correct the bottom error by adding the negative angle to 360 to get the correct result around the whole circle
        return (360.0 + (radians * (180 / Math.PI))).toFixed(2);
    } else {
        return (radians * (180 / Math.PI)).toFixed(2);
    }
}

// convert degrees to radians
function degreesToRadians(degrees){
    return degrees * (Math.PI / 180);
}
// back to reDrawCanvas()

// draw triangles
// receiving angleInDegrees
function drawTriangle(angleInDegrees){

    // first draw the hypotenuse
    context.moveTo(X_CIRCLE_CENTER, Y_CIRCLE_CENTER);

    // need to figure out where the end point is
    // Cosine = Adjacent / Hypotenuse so
    // xEndPoint = xStartPoint + radius * cos(angle) [angle in radians]
    let xEndPoint = X_CIRCLE_CENTER + RADIUS * Math.cos(degreesToRadians(angleInDegrees));

    // Sine = Opposite / Hypotenuse so
    // yEndPoint = yStartPoint + radius * sin(angle) [angle in radians]
    let yEndPoint = Y_CIRCLE_CENTER + RADIUS * -Math.sin(degreesToRadians(angleInDegrees));

    // draw a text in regards to radians on the screen
    drawTextAtPoint("Radians : " + degreesToRadians(angleInDegrees).toFixed(2), 15, 85);

    // continue drawing the line to xEndPoint and the yEndPoint
    context.lineTo(xEndPoint, yEndPoint);

    // actually draw the line
    context.stroke();

    // now need to draw the opposite line
    // Draw Opposite Line
    context.moveTo(xEndPoint, yEndPoint);
    context.lineTo(xEndPoint, 300);
    context.stroke();

    // draw a text at the x and y position on the triangle
    // so that we will be able to see the x and y position on the canvas
    // Draw text for x & y
    drawTextAtPoint("( " + xEndPoint.toFixed(2) + ", " + yEndPoint.toFixed(2) + " )", xEndPoint + 10, yEndPoint - 10);

    // now need to calculate the hypotenuse length
    // Calculate the hypotenuse length (Stays Constant)
    let hypotenuseLength = getLineLength(X_CIRCLE_CENTER, Y_CIRCLE_CENTER, xEndPoint, yEndPoint);
    drawTextAtPoint("Hypotenuse Length : " + hypotenuseLength.toFixed(2), 15, 105);

    // Calculate the opposite length
    let oppositeLength = getLineLength(xEndPoint, yEndPoint, xEndPoint, 300);
    drawTextAtPoint("Opposite Length : " + oppositeLength.toFixed(2), 15, 125);
}


// get line length to calculate the length of the hypotenuse and the opposite line
// Distance = √((x2 - x1)² + (y2 - y1)²)
function getLineLength(x1, y1, x2, y2){
    let xS = x2 - x1;
    xS = xS * xS;
    let yS = y2 - y1;
    yS = yS * yS;

    return Math.sqrt(xS + yS);
}