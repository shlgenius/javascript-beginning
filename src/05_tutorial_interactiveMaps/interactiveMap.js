
// Illustrator
// Place image
// Object -> Artboards -> Fit to Artboard Bounds
// Window -> Image Trace -> 
// Preset : 3 Colors
// View : Tracing Results with Outlines
// Method : Overlapping
// Uncheck Preview -> Trace
// Object -> Image Trace Expand
// Delete extra stuff in layers
// Select all states -> Add outline stroke
// Name every layer using state name [Put Parts of Hawaii / Michigan in one layer]

// Export SVG file
// File -> Export -> Export as -> SVG
// Styling : Internal CSS
// Object IDs : Layer Names
// Decimal : 2

// Array with all state names used by group tags that contain paths
let stateArray = [ 'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware', 'Washington_DC', 'Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','NewHampshire','NewJersey','NewMexico','NewYork','NorthCarolina','NorthDakota','Ohio','Oklahoma','Oregon','Pennsylvania','RhodeIsland','SouthCarolina','SouthDakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming' ];

// 0 : republic win, 1 : democratic win
let results2016 = [0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0];

let results2012 = [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0];

let results2008 = [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0];

let results2004 = [0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0];

let results2000 = [0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0];

let results1996 = [0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0];

function get1996Results(){
    changeColors(results1996);
}

function get2000Results(){
    changeColors(results2000);
}

function get2004Results(){
    changeColors(results2004);
}

function get2008Results(){
    changeColors(results2008);
}

function get2012Results(){
    changeColors(results2012);
}

function get2016Results(){
    changeColors(results2016);
}

function changeColors(arrayYear){
    for(i = 0; i < stateArray.length; i++){

        // target each specific element inside of the svg tag
        let stateElement = document.getElementById(stateArray[i]);

        // now target the path and change the fill color of the tag
        // use childElementCount because actually in the case of hawai and michigan there are 2 paths inside one group
        for(j = 0; j < stateElement.childElementCount; j++){
            if(arrayYear[i]){

                // set an attribute on an element
                stateElement.children[j].setAttribute('style', 'fill: #367DCF');
            } else {
                stateElement.children[j].setAttribute('style', 'fill: #A92B17');
            }
        }
    }
}