// JavaScript Paint App JavaScript Canvas API

// Reference to the canvas element
let canvas;;

// Context provides functions used for drawing and working with Canvas
let ctx;;

// Stores previously drawn image data to restore after new drawings are added
// each time I use a tool need to redraw the canvas
// so sotre all of the previous drawings
let savedImageData;;

// Stores whether I'm currently dragging the mouse
// constantly monitor whether the mouse is being held down or not
// when to draw and when to stop drawing
let dragging = false;

let strokeColor = 'black';

let fillColor = 'black';

let line_Width = 2;

let polygonSides = 6;

// Tool currently using
// set 'brush' by default
let currentTool = 'brush';

let canvasWidth = 600;

let canvasHeight = 600;

// set up all the arrays and information for using brushes
// Stores whether I'm currently using brush
let usingBrush = false;

// hold all of the x points
// Stores line x & ys used to make brush lines
let brushXPoints = new Array();
let brushYPoints = new Array();

// Stores whether mouse is down
let brushDownPos = new Array();



// Stores size data used to create rubber band shapes that will redraw as the user moves the mouse
class ShapeBoundingBox{
    constructor(left, top, width, height) {

        // accept that information
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }
}

// Holds x & y position where clicked
// track the mouse position at all times as well as the mouse down position
// where ever the moused is clicked originally as well as the position of the mouse as it moves around
// on the screen period
class MouseDownPos{
    constructor(x,y) {
        this.x = x,
        this.y = y;
    }
}

// Holds x & y location of the mouse
class Location{
    constructor(x,y) {
        this.x = x,
        this.y = y;
    }
}

// Holds x & y polygon point values
class PolygonPoint{
    constructor(x,y) {
        this.x = x,
        this.y = y;
    }
}

// Stores top left x & y and size of rubber band box 
let shapeBoundingBox = new ShapeBoundingBox(0,0,0,0);

// Holds x & y position where clicked
let mousedown = new MouseDownPos(0,0);

// Holds x & y location of the mouse
let loc = new Location(0,0);

// Call for our function to execute when page is loaded
document.addEventListener('DOMContentLoaded', setupCanvas);

function setupCanvas(){

    // Get reference to canvas element
    canvas = document.getElementById('my-canvas');

    // Get methods for manipulating the canvas
    ctx = canvas.getContext('2d');
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = line_Width;

    // Execute ReactToMouseDown when the mouse is clicked
    canvas.addEventListener("mousedown", ReactToMouseDown);

    // Execute ReactToMouseMove when the mouse is clicked
    canvas.addEventListener("mousemove", ReactToMouseMove);

    // Execute ReactToMouseUp when the mouse is clicked
    canvas.addEventListener("mouseup", ReactToMouseUp);
}

function ChangeTool(toolClicked){
    document.getElementById("open").className = "";
    document.getElementById("save").className = "";
    document.getElementById("brush").className = "";
    document.getElementById("line").className = "";
    document.getElementById("rectangle").className = "";
    document.getElementById("circle").className = "";
    document.getElementById("ellipse").className = "";
    document.getElementById("polygon").className = "";

    // Highlight the last selected tool on toolbar
    document.getElementById(toolClicked).className = "selected";

    // Change current tool used for drawing
    currentTool = toolClicked;
}

// get mouse position based on of getting the canvas size and repositioning based on of the size of the web page
// Get Mouse Position
// Save Canvas Image
// Redraw Canvas Image
// Update Rubberband Size Data
// Get Angle Using X & Y position
// Radians to Degrees
// Degrees to Radians
// Draw Rubberband Shape
// Update Rubberband on Movement
// ReactToMouseDown
// ReactToMouseMove
// ReactToMouseUp
// Save Image
// Open Image


// Get Mouse Position
// Returns mouse x & y position based on canvas position in page
function GetMousePosition(x,y){

    // Get canvas size and position in web page
    let canvasSizeData = canvas.getBoundingClientRect();

    // change based on of size and position of the canvas and relation to the size of the position of the web page
    return {
        x: (x - canvasSizeData.left) * (canvas.width / canvasSizeData.width),
        y: (y - canvasSizeData.top) * (canvas.height / canvasSizeData.height)
    };
}

// Save Canvas Image
function SaveCanvasImage(){
    savedImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
}

// Redraw Canvas Image
function RedrawCanvasImage(){

    // Restore image
    ctx.putImageData(savedImageData,0,0);
}

// Update Rubberband Size Data
// pass the mouse position
function UpdateRubberbandSizeData(loc){

    // figure out what the width and height are for our bounding box
    // Height & width are the difference between were clicked and current mouse position

    // width
    shapeBoundingBox.width = Math.abs(loc.x - mousedown.x);

    // height
    shapeBoundingBox.height = Math.abs(loc.y - mousedown.y);

    // If mouse is below where mouse was clicked originally
    if(loc.x > mousedown.x){

        // Store mousedown because it is farthest left
        shapeBoundingBox.left = mousedown.x;
    } else {

        // Store mouse location because it is most left
        shapeBoundingBox.left = loc.x;
    }

    // If mouse location is below where clicked originally
    if(loc.y > mousedown.y){

        // Store mousedown because it is closer to the top of the canvas
        shapeBoundingBox.top = mousedown.y;
    } else {

        // Otherwise store mouse position
        shapeBoundingBox.top = loc.y;
    }
}

// Get Angle Using X & Y position
// x = Adjacent
// y = Opposite
// Tan(Angle) = Opposite / Adjacent
// Angle = ArcTan(Opposite/Adjacent)
function getAngleUsingXAndY(mouselocX, mouselocY){

    let adjacent = mousedown.x - mouselocX;
    let opposite = mousedown.y - mouselocY;

    return radiansToDegrees(Math.atan2(opposite, adjacent));
}

// Radians to Degrees
function radiansToDegrees(rad){
    return (rad * (180 / Math.PI)).toFixed(2);
}

// Degrees to Radians
// Converts degrees to radians
function degreesToRadians(degrees){
    return degrees * (Math.PI / 180);
}

// polygon draw points, store points, connect those points
function getPolygonPoints(){

    // get the current angle because I have to get the angle based on whatever
    // the x and y location is 
    // Get angle in radians based on x & y of mouse location
    let angle = degreesToRadians(getAngleUsingXAndY(loc.x, loc.y));

    // X & Y for the X & Y point representing the radius is equal to the X & Y of the bounding rubberband box
    let radiusX = shapeBoundingBox.width;
    let radiusY = shapeBoundingBox.height;

    // Stores all points in the polygon
    let polygonPoints = [];
    
    // Each point in the polygon is found by breaking the parts of the polygon into triangles
    // Then I can use the known angle and adjacent side length 
    // to find the X = mouseLoc.x + radiusX * Sin(angle)
    // You find the Y = mouseLoc.y - radiusY * Cos(angle)
    for(let i = 0; i < polygonSides; i++){


        // inside of polygonPoints array push on each of those points
        // create a new polygon point object and then go location x and use the formula
        polygonPoints.push(new PolygonPoint(loc.x + radiusX * Math.sin(angle), loc.y - radiusY * Math.cos(angle)));

        // 2 * PI equals 360 degrees
        // Divide 360 into parts based on how many polygon sides you want
        angle += 2 * Math.PI / polygonSides;
    }
    return polygonPoints;
}

// Get the polygon points and draw the polygon
function getPolygon(){
    let polygonPoints = getPolygonPoints();
    ctx.beginPath();
    ctx.moveTo(polygonPoints[0].x, polygonPoints[0].y);
    for(let i = 1; i < polygonSides; i++){
        ctx.lineTo(polygonPoints[i].x, polygonPoints[i].y);
    }
    ctx.closePath();
}

// Draw Rubberband Shape
// Called to draw the line
function drawRubberbandShape(loc){

    ctx.strokeStyle = strokeColor;
    ctx.fillStyle = fillColor;

    if(currentTool === "brush"){
        DrawBrush();
    } else if(currentTool === "line"){
        // Draw Line
        ctx.beginPath();
        ctx.moveTo(mousedown.x, mousedown.y);
        ctx.lineTo(loc.x, loc.y);
        ctx.stroke();
    } else if(currentTool === "rectangle"){
        // Creates rectangles
        ctx.strokeRect(shapeBoundingBox.left, shapeBoundingBox.top, shapeBoundingBox.width, shapeBoundingBox.height);
    } else if(currentTool === "circle"){
        // Create circles
        let radius = shapeBoundingBox.width;
        ctx.beginPath();
        ctx.arc(mousedown.x, mousedown.y, radius, 0, Math.PI * 2);
        ctx.stroke();
    } else if(currentTool === "ellipse"){
        // Create ellipses
        // ctx.ellipse(x, y, radiusX, radiusY, rotation, startAngle, endAngle)
        let radiusX = shapeBoundingBox.width / 2;
        let radiusY = shapeBoundingBox.height / 2;
        ctx.beginPath();
        ctx.ellipse(mousedown.x, mousedown.y, radiusX, radiusY, Math.PI / 4, 0, Math.PI * 2);
        ctx.stroke();
    } else if(currentTool === "polygon"){
        // Create polygons
        getPolygon();
        ctx.stroke();
    }
        
    // now modify mousemove
}

// Update Rubberband on Movement
// get passed whatever the current mouse location is
function UpdateRubberbandOnMove(loc){
    
    // Stores changing height, width, x & y position of most top left point being either the click or mouse location
    UpdateRubberbandSizeData(loc);

    // Redraw the shape
    drawRubberbandShape(loc);
}

// Store each point as the mouse moves and whether the mouse button is currently being dragged
function AddBrushPoints(x, y, mousedown) {
    brushXPoints.push(x);
    brushYPoints.push(y);

    // Store true that mouse is down
    brushDownPos.push(mousedown);
}

// Cycle through all brush points and connect them with lines
function DrawBrush() {
    for(let i = 1; i < brushXPoints.length; i++) {
        ctx.beginPath();

        // Check if the mouse button was down at this point and if so continue drawing
        if(brushDownPos[i]) {
            ctx.moveTo(brushXPoints[i-1], brushYPoints[i-1]);
        } else {
            ctx.moveTo(brushXPoints[i] - 1, brushYPoints[i]);
        }
        ctx.lineTo(brushXPoints[i], brushYPoints[i]);
        ctx.closePath();
        ctx.stroke();
    }
}


// ReactToMouseDown
// receive the event information on where the mouse was clicked
function ReactToMouseDown(e){

    // Change the mouse pointer to a crosshair
    canvas.style.cursor = "crosshair";

    // Store location 
    // pass inside of it the event information that was provided to me
    loc = GetMousePosition(e.clientX, e.clientY);

    // Save the current canvas image
    SaveCanvasImage();

    // Store mouse position when clicked
    mousedown.x = loc.x;
    mousedown.y = loc.y;

    // Store that yes the mouse is being held down
    dragging = true;

    // TODO HANDLE BRUSH
    // Brush will store points in an array
    if (currentTool === 'brush') {
        usingBrush = true;
        AddBrushPoints(loc.x, loc.y);
    }
}

// ReactToMouseMove
function ReactToMouseMove(e){
    canvas.style.cursor = "crosshair";
    loc = GetMousePosition(e.clientX, e.clientY);

    // TODO HANDLE BRUSH
    // If using brush tool and dragging store each point
    if (currentTool === 'brush' && dragging && usingBrush) {

        // Throw away brush drawings that occur outside of the canvas
        if (loc.x > 0 && loc.x < canvasWidth && loc.y > 0 && loc.y < canvasHeight) {
            AddBrushPoints(loc.x, loc.y);
        }
        RedrawCanvasImage();
        DrawBrush();
    } else {
        if (dragging) {
            RedrawCanvasImage();
            UpdateRubberbandOnMove(loc);
        }
    }

    if(dragging){
        RedrawCanvasImage();
        UpdateRubberbandOnMove(loc);
    }
    // now can draw rectangle
}

// ReactToMouseUp
function ReactToMouseUp(e){
    canvas.style.cursor = "default";
    loc = GetMousePosition(e.clientX, e.clientY);
    RedrawCanvasImage();
    UpdateRubberbandOnMove(loc);
    dragging = false;
    usingBrush = false;
}

// Save Image
// Saves the image in your default download directory
function SaveImage(){

    // Get a reference to the link element 
    var imageFile = document.getElementById("img-file");

    // Set that you want to download the image when link is clicked
    // change the attributes for it
    imageFile.setAttribute('download', 'image.png');

    // Reference the image in canvas for download
    // set the attribute for the reference to the canvas drawing
    imageFile.setAttribute('href', canvas.toDataURL());
}

// Open Image
function OpenImage(){

    // create an image object
    let img = new Image();

    // Once the image is loaded clear the canvas and draw new image
    // define the function you want to actually call once the image is loaded
    img.onload = function(){

        // clear the screen start from the upperlength corner to the canvas width and height
        ctx.clearRect(0,0,canvas.width, canvas.height);

        // draw our imported image
        ctx.drawImage(img,0,0);
    }
    img.src = 'image.png';
}

// checked web page, brush not working properly