// JavaScript was originally designed to write programs that would execute in the browser 
// but now it is used on the server side, with databases, and more


// ==================== CALCULATE PI ====================

// The formula for calculating PI is
// Pi = 4/1 - 4/3 + 4/5 - 4/7 + ...

// A function is a block of code that you give a name to
// You can pass the function a value that will be assigned to the name iterations

// many iterations of the calculation above
// so we are going to ask the user how many iterations you want
function calcPI(iterations){

    // create a constant value that would not change
    // This is a constant which has a value that can't change
    const piVal = 3.14159;

    // You assign names to numbers, text and functions
    // Variable names can't start with a number, contain 
    // spaces, but can contain letters, numbers, underscores
    // or $ (Are case sensitive)

    // notice that JavaScript only has percision up to 16 digits
    // so our value of PI is only going to have a maximum number of decimal places of 16

    // Numbers have 16 digits of precision so our PI
    // can't be bigger than that
    v5 = 0.11111111111111111;
    console.log(v5 + 0.11111111111111111);

    // to devine a variable with JavaScript is with 'let'
    // divisor : the denominator above    
    let pi = 0, divisor = 1;

    // This loop will execute the code between { } as long as the condition is true
    for(i = 0; i <= iterations; i++){
        pi = pi + (4/divisor) - (4/(divisor + 2));
        divisor = divisor + 4;
    }
    
    // now we have to output the result to our browser
    // You can change the value in an HTML element by referring to its id with getElementById()
    // toFixed returns value to 10 decimal places    
    document.getElementById("output1")
        .value = pi.toFixed(10);              // assign the value
        // toFixed(10) : abbreviate the version of PI to 10 digits
}


// ==================== CREATE A FIBONACCI SEQUENCE ====================

// 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...

// create an array
// This array can hold multiple values
let fibList = [];

function getFibList(howMany){
    for(i = 0; i < howMany; i++){
        fibList[i] = fib(i);
    }

    // the 0 should not be there so we need to knock that 0 off of the fornt of the array
    fibList.shift();

    // if you want to knock off the last value of the array
    // fibList.pop();

    // if you want to eliminate a specific value from the array    
    // fibList.splice(3, 2);
    // this will elimiate the value of index 3 and the next value

    document.getElementById("output1")
        .value = fibList.join(", ");
}

function fib(whichNum){
    let num1 = 1, num2 = 0, temp, i = 0;

    while(i < whichNum){
        temp = num1;
        num1 = num1 + num2;
        num2 = temp;
        i++;
    }
    return num2;
}
// function getFibList(howMany){}
// basically going to call another function to generate each of the individual values and store those into an array
// and then output to the output window
    // for(){}
    // we will individually get those items stick them into the cells in the array
    // and call the function that is going to generate each of those individual values Fib

// function fib(whichNum){}
    // while(){}
    // while loop is going to execute code as long as the condition is true 
    // while i is less than the number we want to calculate it will continue calculate
        // temp = num1;
        // store the value that currently is in num1 to temp variable
        // num1 = num1 + num2;
        // num2 = temp;
        // num2 is now going to get the value that was previously stored in temp
        // i++;
        // and then going increment the value of i
    // after calculate it return num2


// ==================== MADLIB GENERATOR ====================

// Create the MadLib using a ~ every place we want to enter a user supplied string
let mLText = "My dear old ~ sat me down to hear some words of wisdom \n 1. Give a man a ~ and you ~ him for a day ~ a man to ~ and he'll ~ forever \n 2. He who ~ at the right time can ~ again \n 3. Always wear ~ ~ in case you're in a ~ \n 4. Don't use your ~ to wipe your ~ Always have a clean ~ with you";

// get each individual word and throw them inside of an array
// and then search the array for the tilda (~)
// and put the input in there and generate a string to ouput on the screen

// Convert the string into an array where spaces determine the individual pieces to put in each array cell
let mLArray = mLText.split(" ");
// Convert the string into an array

// Create array for user input
let inputArray = [];

// main function
function madLibGenerator(){
    createInputArray();

    // If this function returns true we know the user didn't supply new data
    // if the user didn't entered somthing in each of different input areas put an error message out on the screen
    if(checkForMissingInput()){
        document.getElementById("output1").value = "Enter all values above";
    } else {    // If they did supply new data we can create the custom MadLib
        createMLSentence();
    }
}

// now need to create functions that are referenced in the madLibGenerator
function createInputArray(){
    for(i = 0; i <= 13; i++){
        inputArray[i] = document.getElementById("i" + i).value;
    }
}

function checkForMissingInput(){

    // Create an array with the default values
    let defaultArrayVals = ["Person", "Noun", "Verb", "Adjective", "Plural Verb", "Body Part", "Event"];

    for(i=0; i < inputArray.length; i++){

        // Checks to see if input matches any of the defaults
        // If there are no matches a -1 is returned

        // we do not want defaultArrayVals to be inside of our input array
        // if there is a match of one of these words inside the inputArray it is going to spit out a value that is greater than 1
        if(defaultArrayVals.indexOf(inputArray[i]) > -1 ){
            return true;
        }
    }
    return false;
}

// Cycle through all words in our default MadLib and every place there is a ~ put in the user supplied values
function createMLSentence() {
    let arrIndex = 0;
    for (i = 0; i < mLArray.length; i++) {
        let matchIndex = mLArray.indexOf("~");
        mLArray[matchIndex] = inputArray[arrIndex];
        arrIndex++;
    }
    // output information by joining all of the items in the array into our output window
    document.getElementById("output1").value = mLArray.join(" ");
}
